﻿namespace WindowsFormsApplication6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.myan = new System.Windows.Forms.TextBox();
            this.ch = new System.Windows.Forms.TextBox();
            this.eng = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-4, -9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(295, 268);
            this.button1.TabIndex = 0;
            this.button1.Text = "Translate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // myan
            // 
            this.myan.Location = new System.Drawing.Point(100, 70);
            this.myan.Name = "myan";
            this.myan.Size = new System.Drawing.Size(100, 22);
            this.myan.TabIndex = 1;
            this.myan.TextChanged += new System.EventHandler(this.myan_TextChanged);
            // 
            // ch
            // 
            this.ch.Location = new System.Drawing.Point(100, 121);
            this.ch.Name = "ch";
            this.ch.Size = new System.Drawing.Size(100, 22);
            this.ch.TabIndex = 2;
            this.ch.TextChanged += new System.EventHandler(this.ch_TextChanged);
            // 
            // eng
            // 
            this.eng.Location = new System.Drawing.Point(102, 16);
            this.eng.Name = "eng";
            this.eng.Size = new System.Drawing.Size(100, 22);
            this.eng.TabIndex = 3;
            this.eng.TextChanged += new System.EventHandler(this.eng_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "English";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Myanmar";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Chinese";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eng);
            this.Controls.Add(this.ch);
            this.Controls.Add(this.myan);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "C#Translateor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox myan;
        private System.Windows.Forms.TextBox ch;
        private System.Windows.Forms.TextBox eng;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

